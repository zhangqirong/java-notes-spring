package com.zqr.javanotesspring.mayikt.test;

import com.zqr.javanotesspring.mayikt.entity.UserEntity;
import com.zqr.javanotesspring.spring.SpringAopTest;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Hashtable;
import java.util.concurrent.*;

public class MayiTest extends RecursiveAction {

    @Test
    public void test001() throws BrokenBarrierException, InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(10);
        countDownLatch.countDown();
        CyclicBarrier cyclicBarrier = new CyclicBarrier(10);
        cyclicBarrier.await();
        Semaphore semaphore = new Semaphore(10);
        semaphore.acquire();
        semaphore.release();
        ArrayBlockingQueue<String> strings = new ArrayBlockingQueue<>(10);
        strings.offer("12");
        ConcurrentHashMap map = new ConcurrentHashMap<>();
        map.put("string", "123");
        Hashtable<Integer, String> hashtable = new Hashtable<>();
        hashtable.put(1, "1");
    }

    @Override
    protected void compute() {
        MayiTest mayiTest = new MayiTest();
        mayiTest.fork();
        mayiTest.join();
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringAopTest.class);
    }
}
