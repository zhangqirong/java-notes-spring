package com.zqr.javanotesspring.mayikt.lock;

public interface ZQRCallable<V> {
    V call();
}
