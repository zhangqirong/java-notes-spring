package com.zqr.javanotesspring.mayikt.lock;

import java.util.concurrent.locks.LockSupport;

public class ZQRFutureTask<V> implements Runnable {
    private ZQRCallable<V> callable;

    private Thread concurrentThread;
    private V result;
    private Object lock = new Object();

    public ZQRFutureTask(ZQRCallable callable) {
        this.callable= callable;
    }

    @Override
    public void run() {
        result = callable.call();
        synchronized (lock) {
            lock.notify();
        }
        //LockSupport.unpark(concurrentThread);
    }

    public V get() throws InterruptedException {
        if (result != null) {
            return result;
        }
        synchronized (lock) {
            lock.wait();
        }
        //LockSupport.park(concurrentThread);
        return result;
    }
}
