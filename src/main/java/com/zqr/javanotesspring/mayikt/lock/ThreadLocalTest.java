package com.zqr.javanotesspring.mayikt.lock;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class ThreadLocalTest {
    private ThreadLocal<String> threadLocal = new ThreadLocal<>();
    private AtomicInteger atomicInteger = new AtomicInteger();

    @Test
    public void test001() throws InterruptedException {
        int i = atomicInteger.incrementAndGet();
        int andIncrement = atomicInteger.getAndIncrement();

        Thread thread = new Thread(() -> {
            threadLocal.set("测试");
            System.out.println("测试输出");
            threadLocal.remove();
            System.out.println(Thread.currentThread().getName() + "threadLocal: " + threadLocal.get());
        });
        thread.start();
        thread.join();



    }
}
