package com.zqr.javanotesspring.mayikt.lock;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class ZQRLock {

    private volatile AtomicInteger atomicState = new AtomicInteger(0);
    private transient Thread exclusiveOwnerThread;
    private ConcurrentLinkedDeque<Thread> waitThreadDeque = new ConcurrentLinkedDeque();

    public void lock() {
        if (atomicState.compareAndSet(0, 1)) {
            exclusiveOwnerThread = Thread.currentThread();
        } else {

        }
    }

    public void unlock() {

    }
}
