package com.zqr.javanotesspring.mayikt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zqr.javanotesspring.mayikt.entity.StrategyEntity;

public interface StrategyMapper extends BaseMapper<StrategyEntity> {

}
