package com.zqr.javanotesspring.mayikt.controller;

import com.zqr.javanotesspring.mayikt.entity.UserEntity;
import com.zqr.javanotesspring.mayikt.util.JvmCacheUtils;
import com.zqr.javanotesspring.mayikt.util.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@Slf4j
public class UserController {

    @Autowired
    private RedisUtils redisUtils;

    @RequestMapping("/getUser")
    public UserEntity getUser(Integer userId) {
        //1.先查询Jvm内置缓存是否有数据
        String key = "getUser()";
        UserEntity redisUser = redisUtils.getEntity(key, UserEntity.class);
        if (redisUser != null) {
            return redisUser;
        }
        //log.info(">>开始查询一级缓存<<<");
        //UserEntity jvmCacheUserEntity = JvmCacheUtils.getEntity(key, UserEntity.class);
        //if (jvmCacheUserEntity != null) {
        //    // 直接返回缓存对象 并且将该缓存数据缓存到Redis中
        //    redisUtils.putEntity(key, jvmCacheUserEntity);
        //    return jvmCacheUserEntity;
        //}
        //// 2.直接查询数据库db
        //log.info(">>一级缓存中没有数据，查询二级缓存<<<");
        //UserEntity dbUserEntity = userMapper.findByUser(userId);
        //if (dbUserEntity == null) {
        //    log.info(">>数据库DB未查询到数据<<<");
        //    return null;
        //}
        //// 将db的数据缓存到一级缓存中
        //JvmCacheUtils.putEntity(key, dbUserEntity);
        //return dbUserEntity;
        return null;
    }
}
