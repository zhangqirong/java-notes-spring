package com.zqr.javanotesspring.mayikt.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class JvmCacheUtils<K, V> {
    private Map<String, Object> cache = new ConcurrentHashMap<>();

    /**
     * 插入缓存
     * @param key
     * @param value
     */
    public void putCache(String key, Object value) {
        String cacheValue = JSONObject.toJSONString(value);
        cache.put(key, cacheValue);
    }

    /**
     * 获取缓存
     * @param key
     * @return
     */
    public Object getCache(K key) {
        return cache.get(key);
    }
}
