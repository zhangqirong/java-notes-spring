package com.zqr.javanotesspring.mayikt.design.strategy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AliyunStrategy implements Strategy{

    @Override
    public void deal() {

    }
}
