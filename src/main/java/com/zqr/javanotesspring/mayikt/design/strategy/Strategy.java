package com.zqr.javanotesspring.mayikt.design.strategy;

public interface Strategy {
    void deal();
}
