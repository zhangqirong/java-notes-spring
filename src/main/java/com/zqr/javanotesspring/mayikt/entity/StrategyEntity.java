package com.zqr.javanotesspring.mayikt.entity;

import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;

@Data
@TableName("zqr_strategy")
public class StrategyEntity {
    private String id;
    @Getter
    private String strategyName;
    private String strategyId;
    private String strategyBeanId;
    /**
     * 归档
     */
    @TableLogic
    private Integer archiveFlag = 0;

}
