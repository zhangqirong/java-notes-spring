package com.zqr.javanotesspring.mayikt.entity;

import lombok.Data;

@Data
public class UserEntity {
    private Integer userId;
    private String name;
}