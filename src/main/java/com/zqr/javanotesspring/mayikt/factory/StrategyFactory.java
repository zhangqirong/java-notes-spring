package com.zqr.javanotesspring.mayikt.factory;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zqr.javanotesspring.mayikt.entity.StrategyEntity;
import com.zqr.javanotesspring.mayikt.mapper.StrategyMapper;
import com.zqr.javanotesspring.mayikt.util.SpringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class StrategyFactory {
    @Autowired
    private StrategyMapper strategyMapper;

    private Map<String, Class<?>> beans = new ConcurrentHashMap<>();

    /**
     * 获取策略类
     *
     * @param strategyId
     * @param t
     * @param <T>
     * @return
     */
    public <T> T getStrategyBean(String strategyId, Class<T> t) {
        if (StringUtils.isEmpty(strategyId)) {
            return null;
        }
        return SpringUtils.getBean(strategyId, t);
    }

    public <T> T getStrategyBean(String strategyId, String strategyType, Class<T> t) {
        if (StringUtils.isEmpty(strategyId)) {
            return null;
        }
        if (StringUtils.isEmpty(strategyType)) {
            return null;
        }
        Class<?> aClass = beans.get(t);
        if (Objects.nonNull(aClass)) {
            return (T) aClass;
        }
        QueryWrapper<StrategyEntity> queryStrategyWrapper = new QueryWrapper<>();
        queryStrategyWrapper.eq("strategy_ID", strategyId);
        queryStrategyWrapper.eq("strategy_type", strategyType);
        StrategyEntity strategyEntity = strategyMapper.selectOne(queryStrategyWrapper);
        if (Objects.isNull(strategyEntity)) {
            return null;
        }
        String strategyBeanId = null;
        if (StringUtils.isEmpty(strategyBeanId)) {
            return null;
        }
        T bean = SpringUtils.getBean(strategyBeanId, t);
        if (Objects.isNull(bean)) {
            //存放到HashMap集合中
            beans.put(strategyId, (Class<?>) bean);
        }
        return bean;
    }
}
