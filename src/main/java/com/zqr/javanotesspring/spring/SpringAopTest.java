package com.zqr.javanotesspring.spring;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class SpringAopTest {

    @Pointcut("execution(* com.zqr.*.*)")
    public void execute() {

    }

    @Before("execute()")
    public void before() {

    }
}
