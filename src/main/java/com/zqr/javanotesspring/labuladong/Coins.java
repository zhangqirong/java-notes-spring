package com.zqr.javanotesspring.labuladong;

import com.sun.org.apache.bcel.internal.generic.NEW;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class Coins {

    public Map<Integer, Integer> memo = new HashMap<>();

    @Test
    public void test001() {

    }

    // coins 中是可选硬币面值，amount 是目标金额
    int coinChange(int[] coins, int amount) {

        if (memo.containsKey(amount)) {
            return memo.get(amount);
        }

        //base case
        if (coins.length == 0) {
            return 0;
        }
        if (coins.length < 0) {
            return -1;
        }

        int res = Integer.MAX_VALUE;

        for (int coin : coins) {
            int subProblem = coinChange(coins, amount - coin);
            if (subProblem == -1) {
                continue;
            }
            if (res < amount) {
                res = Math.min(res, subProblem);
            }
        }
        res = res != Integer.MAX_VALUE ? res : -1;
        memo.put(amount, res);
        return res;
    }

}
