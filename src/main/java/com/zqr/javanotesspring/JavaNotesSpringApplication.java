package com.zqr.javanotesspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaNotesSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaNotesSpringApplication.class, args);
    }

}
