package com.zqr.javanotesspring.designpatterns.factory.simplefactory;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CheesePizza extends Pizza {
    @Override
    public void prepare() {
        log.info("准备做芝士pizza");
    }
}
