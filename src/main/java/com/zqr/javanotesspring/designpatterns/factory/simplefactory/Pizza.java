package com.zqr.javanotesspring.designpatterns.factory.simplefactory;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class Pizza {
    /**
     * 名称
     */
    @Setter
    @Getter
    private String name;

    public abstract void prepare();

    public void bake() {
        log.info("烤【{}】pizza", name);
    }

    public void cut() {
        log.info("切【{}】pizza", name);
    }

    public void box() {
        log.info("打包【{}】pizza", name);
    }

}
