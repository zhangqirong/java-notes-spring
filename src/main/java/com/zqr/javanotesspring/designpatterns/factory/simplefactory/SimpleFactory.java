package com.zqr.javanotesspring.designpatterns.factory.simplefactory;

import java.util.Objects;

public class SimpleFactory {

    public static final String CHEESE = "cheese";
    public static final String PEPPER = "pepper";

    public static Pizza createPizza(String pizzaType) {
        Pizza pizza = null;
        switch (pizzaType) {
            case CHEESE:
                pizza = new CheesePizza();
                pizza.setName(CHEESE);
                break;
            case PEPPER:
                pizza = new PepperPizza();
                pizza.setName(PEPPER);
                break;
            default:

        }
        return pizza;
    }
}
