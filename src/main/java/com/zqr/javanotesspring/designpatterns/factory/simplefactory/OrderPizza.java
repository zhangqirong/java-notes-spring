package com.zqr.javanotesspring.designpatterns.factory.simplefactory;

public class OrderPizza {
    public static void main(String[] args) {
        Pizza cheese = SimpleFactory.createPizza("pepper");
        cheese.prepare();
        cheese.bake();
        cheese.cut();
        cheese.box();
    }
}
