package com.zqr.javanotesspring.designpatterns.factory.simplefactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PepperPizza extends Pizza {

    @Override
    public void prepare() {
        log.info("准备做胡椒pizza");
    }
}
