package com.zqr.javanotesspring.designpatterns.proxy;

import com.zqr.javanotesspring.designpatterns.factory.simplefactory.CheesePizza;
import com.zqr.javanotesspring.designpatterns.factory.simplefactory.Pizza;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@Slf4j
public class JdkProxy implements InvocationHandler {

    private Object target;

    public JdkProxy(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log.info("jdk动态代理前");
        Object invoke = method.invoke(target, args);
        log.info("jdk动态代理后");
        return invoke;
    }

    public static void main(String[] args) {
        CheesePizza cheesePizza = new CheesePizza();
        JdkProxy jdkProxy = new JdkProxy(cheesePizza);
        Pizza pizza = (Pizza) Proxy.newProxyInstance(CheesePizza.class.getClassLoader(), CheesePizza.class.getInterfaces(), jdkProxy);
        pizza.prepare();
    }
}
