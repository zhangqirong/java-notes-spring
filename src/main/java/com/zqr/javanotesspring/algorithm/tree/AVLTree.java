package com.zqr.javanotesspring.algorithm.tree;

import lombok.Data;

public class AVLTree {
    public boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        }
        if (!isBalanced(root.left) || !isBalanced(root.right)) {
            return false;
        }
        if (Math.abs(height(root.left) - height(root.right)) > 1) {
            return false;
        }
        return true;
    }

    public int height(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int l = height(root.left) + 1;
        int r = height(root.right) + 1;
        return Math.max(l, r);
    }

    @Data
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}
