package com.zqr.javanotesspring.algorithm.tree;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class BinaryTree {
    /**
     * 根节点
     */
    private Node root;

    public static void main(String[] args) {
        BinaryTree binaryTree = new BinaryTree();
        Node root = new Node(1, "宋江");
        Node node2 = new Node(2, "吴用");
        Node node3 = new Node(3, "卢俊义");
        Node node4 = new Node(4, "林冲");
        Node node5 = new Node(5, "关胜");

        root.setLeft(node2);
        root.setRight(node3);
        node3.setLeft(node4);
        node3.setRight(node5);
        binaryTree.setRoot(root);

//        binaryTree.preOrder(binaryTree);

        Node node = binaryTree.preOrderSearch(5, binaryTree);
        log.info("node的值为{}", node);
    }

    public void deleteNode(int no) {
        if (root == null) {
            log.info("空树，不能删除");
            return;
        }
        if (root.getNo() == no) {
            root = null;
            return;
        }
        root.deleteNode(no);
    }

    public Node preOrderSearch(int no, BinaryTree binaryTree) {
        return preOrderSearch(no, binaryTree.getRoot());
    }


    public Node preOrderSearch(int no, Node currentNode) {
        Node resultNode;
        if (currentNode.getNo() == no) {
            return currentNode;
        }
        if (currentNode.left != null) {
            resultNode = preOrderSearch(no, currentNode.getLeft());
            if (resultNode != null) {
                return resultNode;
            }
        }
        if (currentNode.right != null) {
            resultNode = preOrderSearch(no, currentNode.getRight());
            if (resultNode != null) {
                return resultNode;
            }
        }
        return null;
    }

    public Node infixOrderSearch(int no, Node currentNode) {
        Node resultNode;
        if (currentNode.left != null) {
            resultNode = preOrderSearch(no, currentNode.getLeft());
            if (resultNode != null) {
                return resultNode;
            }
        }
        if (currentNode.getNo() == no) {
            return currentNode;
        }
        if (currentNode.right != null) {
            resultNode = preOrderSearch(no, currentNode.getRight());
            if (resultNode != null) {
                return resultNode;
            }
        }
        return null;
    }

    public Node postOrderSearch(int no, Node currentNode) {
        Node resultNode;
        if (currentNode.left != null) {
            resultNode = preOrderSearch(no, currentNode.getLeft());
            if (resultNode != null) {
                return resultNode;
            }
        }

        if (currentNode.right != null) {
            resultNode = preOrderSearch(no, currentNode.getRight());
            if (resultNode != null) {
                return resultNode;
            }
        }
        if (currentNode.getNo() == no) {
            return currentNode;
        }
        return null;
    }

    /**
     * 前序遍历
     *
     * @param binaryTree 树
     */
    public void preOrder(BinaryTree binaryTree) {
        preOrder(binaryTree.getRoot());
    }

    /**
     * 中序序遍历
     *
     * @param binaryTree 树
     */
    public void infixOrder(BinaryTree binaryTree) {
        infixOrder(binaryTree.getRoot());
    }

    /**
     * 后序遍历
     *
     * @param binaryTree 树
     */
    public void postOrder(BinaryTree binaryTree) {
        postOrder(binaryTree.getRoot());
    }

    public void preOrder(Node currentNode) {
        if (currentNode == null) {
            log.info("二叉树为空，不进行前序遍历");
            return;
        }

        log.info("当前节点，{}", currentNode);
        if (currentNode.left != null) {
            preOrder(currentNode.left);
        }
        if (currentNode.right != null) {
            preOrder(currentNode.right);
        }
    }

    public void infixOrder(Node currentNode) {
        if (currentNode == null) {
            log.info("二叉树为空，不进行中序遍历");
            return;
        }
        //先输出左子树
        if (currentNode.left != null) {
            preOrder(currentNode.left);
        }
        log.info("当前节点，{}", currentNode);
        if (currentNode.right != null) {
            preOrder(currentNode.right);
        }
    }

    public void postOrder(Node currentNode) {
        if (currentNode == null) {
            log.info("二叉树为空，不进行后序遍历");
            return;
        }
        //先输出左子树
        if (currentNode.left != null) {
            preOrder(currentNode.left);
        }
        if (currentNode.right != null) {
            preOrder(currentNode.right);
        }
        log.info("当前节点，{}", currentNode);
    }


    @Data
    @RequiredArgsConstructor
    @ToString(exclude = {"left", "right"})
    static class Node {
        /**
         * 编号
         */
        @NonNull
        private int no;
        /**
         * 数据
         */
        @NonNull
        private Object data;
        /**
         * 左节点
         */
        private Node left;
        /**
         * 右节点
         */
        private Node right;

        public void deleteNode(int no) {
            if (this.left != null && this.left.getNo() == no) {
                this.left = null;
                return;
            }
            if (this.right != null && this.right.getNo() == no) {
                this.right = null;
                return;
            }
            if (this.left != null) {
                this.left.deleteNode(no);
            }
            if (this.right != null) {
                this.right.deleteNode(no);
            }
        }
    }
}
