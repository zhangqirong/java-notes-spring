package com.zqr.javanotesspring.algorithm.tree;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 哈夫曼数
 */
@Slf4j
public class HuffmanTree {
    public static void main(String[] args) {
        int[] arr = {13, 7, 8, 3, 29, 6, 1};
        Node huffmanTree = createHuffmanTree(arr);
        preOrder(huffmanTree);
    }

    /**
     * 前置遍历
     *
     * @param node
     */
    public static void preOrder(Node node) {
        if (node != null) {
            node.preOrder();
            return;
        }
        log.info("是空树，不能遍历");
    }

    public static Node createHuffmanTree(int[] arr) {

        List<Node> nodeList = new ArrayList<>();
        for (int value : arr) {
            nodeList.add(new Node(value));
        }

        while (nodeList.size() > 1) {
            //排序
            Collections.sort(nodeList);
            log.info("nodeList = {}", nodeList);

            Node leftNode = nodeList.get(0);
            Node rightNode = nodeList.get(1);

            Node parentNode = new Node(leftNode.getValue() + rightNode.getValue());
            parentNode.setLeft(leftNode);
            parentNode.setRight(rightNode);
            nodeList.remove(leftNode);
            nodeList.remove(rightNode);
            nodeList.add(parentNode);

        }

        return nodeList.get(0);
    }


    @Data
    @RequiredArgsConstructor
    @ToString(exclude = {"left", "right"})
    static class Node implements Comparable<Node> {
        /**
         * 数据
         */
        @NonNull
        private int value;
        /**
         * 左节点
         */
        private Node left;
        /**
         * 右节点
         */
        private Node right;

        @Override
        public int compareTo(Node node) {
            return this.value - node.value;
        }

        /**
         * 前序遍历
         */
        public void preOrder() {
            log.info("当前节点为={}", this);
            if (this.left != null) {
                this.left.preOrder();
            }
            if (this.right != null) {
                this.right.preOrder();
            }
        }
    }
}
