package com.zqr.javanotesspring.algorithm.tree;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
public class ThreadedBinaryTree {

    public static final int TYPE_1 = 1;
    private Node root;
    private Node pre;

    public void threadedList() {
        Node node = root;
        while (node != null) {

            while (node.getLeftType() == 0) {
                node = node.getLeft();
            }
            System.out.println(node);
            while (node.getRightType() == 1) {
                node = node.getRight();
                System.out.println(node);
            }
            node = node.getRight();
        }
    }

    /**
     * 对二叉树进行中序线索化
     *
     * @param node
     */
    public void threadedNodes(Node node) {
        //1. 左子树
        threadedNodes(node.getLeft());
        //2. 当前节点
        if (node.getLeft() == null) {
            node.setLeft(pre);
            node.setLeftType(TYPE_1);
        }
        if (pre != null && pre.getRight() == null) {
            pre.setRight(node);
            pre.setRightType(TYPE_1);
        }
        pre = node;
        //3. 右子树
        threadedNodes(node.getRight());
    }

    @Data
    @RequiredArgsConstructor
    @ToString(exclude = {"left", "right"})
    static class Node {
        /**
         * 编号
         */
        @NonNull
        private int no;
        /**
         * 数据
         */
        @NonNull
        private Object data;
        /**
         * 左节点
         */
        private Node left;
        /**
         * 右节点
         */
        private Node right;
        /**
         * 0:左子树,1:前驱节点
         */
        private int leftType;
        /**
         * 0:右子树,1:后继节点
         */
        private int rightType;
    }
}
