package com.zqr.javanotesspring.algorithm.tree;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

/**
 * 二叉排序树
 */
@Slf4j
public class BinarySortTree {
    /**
     * 根节点
     */
    private Node root;


    public void add(Node node) {
        if (root == null) {
            root = node;
        } else {
            root.add(node);
        }
    }

    /**
     * 中序遍历
     */
    public void infixOrder() {
        if (root != null) {
            root.infixOrder();
        }else {
            log.info("是空树，不进行中序遍历");
        }
    }


    @Data
    @RequiredArgsConstructor
    @ToString(exclude = {"left", "right"})
    static class Node {
        /**
         * 数值
         */
        @NonNull
        private int value;
        /**
         * 左节点
         */
        private Node left;
        /**
         * 右节点
         */
        private Node right;

        /**
         * 添加节点
         *
         * @param node 节点
         */
        public void add(Node node) {
            if (node == null) {
                return;
            }
            if (node.value < this.value) {
                if (this.left == null) {
                    this.left = node;
                } else {
                    //递归向左子树添加
                    this.left.add(node);
                }
            } else {
                if (this.right == null) {
                    this.right = node;
                } else {
                    //递归向右子树添加
                    this.right.add(node);
                }
            }
        }

        /**
         * 中序遍历
         */
        public void infixOrder() {
            if (this.left != null) {
                this.left.infixOrder();
            }
            log.info("当前节点为：{}", this);
            if (this.right != null) {
                this.right.infixOrder();
            }
        }
    }
}
