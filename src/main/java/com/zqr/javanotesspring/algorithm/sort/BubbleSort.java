package com.zqr.javanotesspring.algorithm.sort;

import lombok.extern.slf4j.Slf4j;

/**
 * 冒泡排序
 */
@Slf4j
public class BubbleSort {
    public static void main(String[] args) {
//        int[] arr = new int[]{-2, 9, -8, 19, 20};

        int[] arr1 = new int[1000];
        for (int i = 0; i < 1000; i++) {
            arr1[i] = (int) (Math.random() * 1000);
        }
        log.info("1000个随机数组，{}", arr1);


        bubble(arr1);
    }

    public static void bubble(int[] arr) {
        int temp;
        boolean flag = false;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j + 1] < arr[j]) {
                    flag = true;
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
            log.info("第{}轮打印数组", i + 1);
            log.info("排序后的数组，{}", arr);
            if (!flag) {
                break;
            } else {
                flag = false;
            }
        }
    }
}
