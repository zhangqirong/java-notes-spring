package com.zqr.javanotesspring.algorithm.sort;


import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

/**
 * 基数排序
 */
@Slf4j
public class BaseSort {
    public static void main(String[] args) {
        int[] arr = {1, 4, 5, 12, 35, 666, 2342, 123, 46};
        baseSort(arr);
        log.info("排序后{}", Arrays.toString(arr));
    }

    public static void baseSort(int[] arr) {
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }

        int maxLength = (max + "").length();

        int[][] bucket = new int[10][arr.length];
        //桶元素计数
        int[] bucketElementCount = new int[10];

        for (int k = 0, n = 1; k < maxLength; k++, n *= 10) {
            //将每个数字的各位放入各自对应的桶中
            for (int i = 0; i < arr.length; i++) {
                int element = arr[i] / n % 10;
                bucket[element][bucketElementCount[element]] = arr[i];
                bucketElementCount[element]++;
            }

            int index = 0;
            for (int i = 0; i < bucket.length; i++) {
                if (bucketElementCount[i] != 0) {
                    for (int j = 0; j < bucketElementCount[i]; j++) {
                        arr[index++] = bucket[i][j];
                    }
                }
                bucketElementCount[i] = 0;
            }
            log.info("第{}排序之后,{}", k + 1, Arrays.toString(arr));
        }

    }
}
