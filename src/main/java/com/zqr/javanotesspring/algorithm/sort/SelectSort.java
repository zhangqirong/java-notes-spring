package com.zqr.javanotesspring.algorithm.sort;

import lombok.extern.slf4j.Slf4j;

/**
 * 选择排序
 */
@Slf4j
public class SelectSort {
    public static void main(String[] args) {
        int[] arr = new int[]{-2, 9, -8, 19, 20, 1};
        select(arr);
    }

    public static void select(int[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            int minIndex = i;
            int min = arr[i];
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < min) {
                    min = arr[j];
                    minIndex = j;
                }
            }
            //将最小的和0位置坐交换
            if (minIndex != i) {
                arr[minIndex] = arr[i];
                arr[i] = min;
            }
            log.info("第{}轮打印数组", i + 1);
            log.info("排序后的数组，{}", arr);
        }
    }
}
