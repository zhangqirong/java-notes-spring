package com.zqr.javanotesspring.algorithm.sort;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

/**
 * 堆排序
 */
@Slf4j
public class HeapSort {
    public static void main(String[] args) {
        int num = 8000000;
        //int[] arr = {4, 6, 8, 5, 9};
        int[] arr = new int[num];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * num);
        }
        //log.info("排序前={}", arr);
        long start = System.currentTimeMillis();
        heapSort(arr);
        long end = System.currentTimeMillis();
        //log.info("排序后={}", arr);
        log.info("用时={}", (end - start));
    }

    public static void heapSort(int[] arr) {
        log.info("开始堆排序");
        int temp = 0;

        //将无序序列调整为一个大顶堆
        for (int i = arr.length / 2 - 1; i >= 0; i--) {
            adjustHeap(arr, i, arr.length);
        }

        for (int i = arr.length - 1; i > 0; i--) {
            temp = arr[i];
            arr[i] = arr[0];
            arr[0] = temp;
            adjustHeap(arr, 0, i);
        }
        log.info("数组={}", Arrays.toString(arr));
    }

    /**
     * 将局部二叉树（数组）调整为大顶堆
     *
     * @param arr    待调整的数组
     * @param i      非叶子节点在数组中的索引
     * @param length 对多个元素调整，length在减少
     */
    public static void adjustHeap(int[] arr, int i, int length) {
        int temp = arr[i];
        //第一次循环是根节点，第二次循环就是当前节点的左子树
        for (int k = i * 2 + 1; k < length; k = k * 2 + 1) {
            if (k + 1 < length && arr[k] < arr[k + 1]) {
                //将k指向右子树，说明右子树比较大
                k++;
            }
            if (arr[k] > temp) {
                arr[i] = arr[k];
                i = k;
            } else {
                break;
            }
        }
        arr[i] = temp;
    }
}
