package com.zqr.javanotesspring.algorithm.sort;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class QuickSort {
    public static void main(String[] args) {
        int[] arr = new int[]{-2, 9, -8, 19, 20, 1};
        quickSort(arr, 0, arr.length - 1);
        log.info("arr排序后，arr:{}", arr);
    }

    public static void quickSort(int[] arr, int leftIndex, int rightIndex) {
        int l = leftIndex;
        int r = rightIndex;
        int pivot = (leftIndex + rightIndex) / 2;
        int temp;

        while (l < r) {

            while (arr[l] < arr[pivot]) {
                l += 1;
            }

            while (arr[pivot] < arr[r]) {
                r -= 1;
            }

            if (l >= r) {
                break;
            }

            temp = arr[l];
            arr[l] = arr[r];
            arr[r] = temp;

            //中间值和左边相等
            if (arr[l] == arr[pivot]) {
                r -= 1;
            }
            //中间值和右边相等
            if (arr[pivot] == arr[r]) {
                l += 1;
            }
        }

        //中间值有可能几个值是相等
        if (l == r) {
            l += 1;
            r -= 1;
        }

        //左递归
        if (leftIndex < r) {
            quickSort(arr, leftIndex, r);
        }

        //右递归
        if (l < rightIndex) {
            quickSort(arr, l, rightIndex);
        }
    }
}
