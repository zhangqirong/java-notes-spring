package com.zqr.javanotesspring.algorithm.sort;

import java.util.Arrays;

/**
 * 归并排序
 */
public class MergeSort {
    public static void main(String[] args) {
        int[] array = new int[]{12, 3, 46, 7, 34, 1, -9, -21};
        int[] temp = new int[array.length];
        mergeSort(array, 0, array.length - 1, temp);
        System.out.println("归并排序后======"+Arrays.toString(array));
    }

    public static void mergeSort(int[] array, int left, int right, int[] temp) {
        if (left < right) {
            int mid = (left + right) / 2;
            //向左递归拆分
            mergeSort(array, left, mid, temp);
            //向右递归拆分
            mergeSort(array, mid + 1, right, temp);
            merge(array, left, mid, right, temp);
        }
    }

    public static void merge(int[] array, int left, int mid, int right, int[] temp) {
        System.out.println("嘿嘿嘿");
        int l = left;
        int j = mid + 1;
        int t = 0;

        //将左右两个有序的数据按照
        while (l <= mid && j <= right) {
            if (array[l] <= array[j]) {
                temp[t] = array[l];
                l++;
                t++;
            } else {
                temp[t] = array[j];
                j++;
                t++;
            }
        }

        //将剩余的数据依次都填放到temp上面
        while (l <= mid) {
            temp[t] = array[l];
            l++;
            t++;
        }

        while (j <= right) {
            temp[t] = array[j];
            j++;
            t++;
        }

        //将temp元素拷贝会array
        //并不是每次都是拷贝所有
        t = 0;
        int tempLeft = left;
        while (tempLeft <= right) {
            array[tempLeft] = temp[t];
            tempLeft++;
            t++;
        }

    }
}
