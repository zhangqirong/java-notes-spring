package com.zqr.javanotesspring.algorithm.sort;

import lombok.extern.slf4j.Slf4j;

/**
 * 插入排序
 */
@Slf4j
public class InsertSort {
    public static void main(String[] args) {
        int[] arr = new int[]{-2, 9, -8, 19, 20, 1};
        insert(arr);
    }

    public static void insert(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int insertVal = arr[i];
            int insertIndex = i - 1;
            while (insertIndex >= 0 && arr[insertIndex] > insertVal) {
                arr[insertIndex + 1] = arr[insertIndex];
                insertIndex--;
            }
            arr[insertIndex + 1] = insertVal;
            log.info("第{}轮打印数组", i + 1);
            log.info("排序后的数组，{}", arr);
        }


    }
}
