package com.zqr.javanotesspring.algorithm;

public class Count3Quit1 {
    public static void main(String[] args) {
        count3Quit1(10);
    }

    public static void count3Quit1(int totalPeople) {
        boolean[] people = new boolean[totalPeople];
        for (int i = 0; i < totalPeople; i++) {
            people[i] = true;
        }

        int leftPeople = totalPeople;
        int index = 0;
        int count = 0;

        while (leftPeople > 1) {
            if (people[index]) {
                count++;
                if (count == 3) {
                    people[index] = false;
                    leftPeople--;
                    count = 0;
                }
            }

            index++;

            if (index > totalPeople - 1) {
                index = 0;
            }
        }

        for (int i = 0; i < people.length; i++) {
            if (people[i]) {
                System.out.println("最后剩下的位置为" + (i + 1));
            }
        }
    }
}
