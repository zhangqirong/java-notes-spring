package com.zqr.javanotesspring.algorithm.search;

public class Solution {

    public static void main(String[] args) {
        int [] arr ={4 ,5, 1, 2, 3};
        System.out.println(binarySearch2(arr,0,arr.length-1,3));
    }

    public static int binarySearch(int[] arr, int left, int right, int findVal) {

        if (left > right) {
            return -1;
        }

        int mid = (left + right) / 2;
        int midVal = arr[mid];

        if (findVal > midVal) {
            return binarySearch(arr, mid + 1, right, findVal);
        }
        if (findVal < midVal) {
            return binarySearch(arr, left, mid - 1, findVal);
        }
        return mid;
    }


    public static int binarySearch2(int[] arr, int left, int right, int findVal) {
        if (arr.length == 0) {
            return 0;
        }
        if (arr[left] < arr[right]) {
            return arr[left];
        }
        if (findVal < arr[right]) {
            return findVal;
        }
        if (left > right) {
            return -1;
        }
        int mid = (left + right) / 2;
        int midVal = arr[mid];

        if (right > midVal) {
            return binarySearch2(arr, left, mid-1 , arr[mid]);
        }
        if (right < midVal) {
            return binarySearch2(arr, mid +1 , right, arr[mid]);
        }
        return findVal;
    }

}
